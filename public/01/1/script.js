$(document).ready(function() {

	$('.ajax-popup-link').magnificPopup({
			type: 'ajax',
			callbacks: {
				ajaxContentAdded: (function() {
					$('.ajcol').slick({
						dots:true,
					});
				})

			}
	});

	function initSliders() {
		$('.ajcol').slick();
		console.log("slick open");
	};
});